# Assignment05 - Console-based SQL Application

This assignment consists of two parts, of which both involve using SQL queries to build or interact with a database.

## Part A - SuperHeroDB

9 SQL queries were made in order to build a database containing superheroes, superpowers and assistants and their corresponding relations and keys.

### Usage:

Run each .sql file in ascending order. It is ensured these SQL files interact with the correct database.

## Part B - Interaction with the Chinook Database

There is interaction with a Database representing an iTunes clone. Through SQL queries information about customers within the database is found.

### Usage:

- After forking/ cloning the project, run the solution Assignment05.sln.
- In the main class Program.cs there is a method called InitConnectionString. 
- Make sure to enter the name of your local environment as 'builder.DataSource'. This has been noted as @"Please Enter Your DataSource Here".
- Now you can run the application. The console will prompt you with the available options and expected input.

## Project Collaborators:

Linh Trieu: https://gitlab.com/linhmillion
Brian Huynen: https://gitlab.com/brianhuynen
