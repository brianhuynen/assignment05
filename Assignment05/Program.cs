﻿using Microsoft.Data.SqlClient;

namespace Assignment05
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SQLHelper helper = new(InitConnectionString());

            bool isRunning = true;
            while (isRunning)
            {
                Console.WriteLine("Your action:\n" +
                    " 1 - Add a Customer\n" +
                    " 2 - Get all Customers from Database\n" +
                    " 3 - Get a Customer by ID\n" +
                    " 4 - Get a Customer by name\n" +
                    " 5 - Get a range of Customers\n" +
                    " 6 - Update Customer\n" +
                    " 7 - Show Country distribution among Customers\n" +
                    " 8 - Show Customers who spent the most\n" +
                    " 9 - Show most popular genre for a Customer\n" +
                    "10 - Exit application\n" +
                    $"DB Size: {helper.DbSize}");

                string input = Console.ReadLine();
                Int32.TryParse(input, out int choice);

                switch (choice)
                {
                    case 1:
                        helper.HandleAddCustomer();
                        break;
                    case 2:
                        helper.HandleGetCustomer("all");
                        break;
                    case 3:
                        helper.HandleGetCustomer("id");
                        break;
                    case 4:
                        helper.HandleGetCustomer("name");
                        break;
                    case 5:
                        helper.HandleGetCustomer("range");
                        break;
                    case 6:
                        helper.HandleUpdateCustomer();
                        break;
                    case 7:
                        helper.CustomerCountryDistribution();
                        break;
                    case 8:
                        helper.CustomerHighestSpender();
                        break;
                    case 9:
                        helper.CustomerMostPopularGenre();
                        break;
                    case 10:
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid Number");
                        break;
                }
            }
        }
        /// <summary>
        /// Function that creates the connection string used to communicate with the database.
        /// </summary>
        /// <returns>The full connection string that embeds the local environment and the database to interact with</returns>
        static string InitConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = @"Please Enter Your DataSource Here"; //URL as instantiated in SSMS
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;

            return builder.ConnectionString;
        }
    }
}