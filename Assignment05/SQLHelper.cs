﻿using Microsoft.Data.SqlClient;
using Assignment05.Models;

namespace Assignment05
{
    public class SQLHelper : ICustomerRepository
    {
        private string connectionString;
        public int DbSize { get; set; } = 0;
        public SQLHelper(string connectionString)
        {
            this.connectionString = connectionString;
            DbSize = GetDBSize();
        }
        /// <summary>
        /// Helper function to get the size of the database
        /// </summary>
        /// <returns>An integer representing the size of the database</returns>
        public int GetDBSize()
        {
            int cnt = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");               
                    try
                    {
                        string readSQL = $"SELECT CustomerId, {GetCustomerDataQueryString()} FROM Customer";
                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    cnt++;
                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
            return cnt;
        }
        /// <summary>
        /// Helper function that updates the stored database size which is called after successfully adding a customer
        /// </summary>
        public void UpdateDBCount()
        {
            this.DbSize++;
        }
        /// <summary>
        /// Helper function that handles getting the data for a customer when adding a new Customer to the database.
        /// </summary>
        public void HandleAddCustomer()
        {
            Console.WriteLine("Enter First Name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter Last Name:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter Country:");
            string country = Console.ReadLine();
            Console.WriteLine("Enter Postal Code:");
            string postalCode = Console.ReadLine();
            Console.WriteLine("Enter Phone Number:");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine("Enter Email:");
            string email = Console.ReadLine();

            Customer newCustomer = new(firstName, lastName, country, postalCode, phoneNumber, email);
            AddCustomer(newCustomer);
        }
        /// <summary>
        /// Helper function that handles getting the data for updating an existing Customer in the database.
        /// </summary>
        public void HandleUpdateCustomer()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");

                    Console.WriteLine("Enter Id:");
                    string input = Console.ReadLine();
                    Int32.TryParse(input, out int id);

                    Console.WriteLine("Enter First Name:");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("Enter Last Name:");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("Enter Country:");
                    string country = Console.ReadLine();
                    Console.WriteLine("Enter Postal Code:");
                    string postalCode = Console.ReadLine();
                    Console.WriteLine("Enter Phone Number:");
                    string phoneNumber = Console.ReadLine();
                    Console.WriteLine("Enter Email:");
                    string email = Console.ReadLine();

                    Customer newCustomer = new(firstName, lastName, country, postalCode, phoneNumber, email);

                    try
                    {
                        string updateSQL = $"UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                                           $"PostalCode = @PostalCode, Phone = @PhoneNumber, Email = @Email WHERE CustomerId = {id}";

                        using (SqlCommand command = new SqlCommand(updateSQL, connection))
                        {
                            command.Parameters.AddWithValue("@FirstName", newCustomer.FirstName);
                            command.Parameters.AddWithValue("@LastName", newCustomer.LastName);
                            command.Parameters.AddWithValue("@Country", newCustomer.Country);
                            command.Parameters.AddWithValue("@PostalCode", newCustomer.PostalCode);
                            command.Parameters.AddWithValue("@PhoneNumber", newCustomer.PhoneNumber);
                            command.Parameters.AddWithValue("@Email", newCustomer.Email);

                            command.ExecuteNonQuery();
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that handles the various ways of retrieving information about a customer from the database.
        /// </summary>
        /// <param name="action">A string representing the action to perform.</param>
        public void HandleGetCustomer(string action)
        {
            string input;
            switch(action)
            {
                case "all":
                    GetAllCustomers(0);
                    break;
                case "id":
                    Console.WriteLine("Enter Id:");
                    input = Console.ReadLine();
                    Int32.TryParse(input, out int id);
                    GetCustomerById(id);
                    break;
                case "name":
                    Console.WriteLine("Enter Name:");
                    input = Console.ReadLine();
                    GetCustomerByName(input);
                    break;
                case "range":
                    Console.WriteLine("Enter starting ID:");
                    input = Console.ReadLine();
                    Int32.TryParse(input, out int page);
                    Console.WriteLine("Enter amount of customers to display:");
                    input = Console.ReadLine();
                    Int32.TryParse(input, out int pageSize);
                    GetAllCustomers(page, pageSize);
                    break;
                default:
                    Console.WriteLine("Unknown Action");
                    break;
            }
        }
        /// <summary>
        /// Helper function that retrieves a customer from the database by name or checks whether there exists a customer starting with the input phrase.
        /// </summary>
        /// <param name="name">The first name of the customer or a part thereof.</param>
        public void GetCustomerByName(string name)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");
                    try
                    {
                        string readSQL = $"SELECT {GetCustomerDataQueryString()} FROM Customer WHERE FirstName LIKE \'{name}%\'";

                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                try
                                {
                                    reader.GetValue(0);

                                    while (reader.Read())
                                    {
                                        Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                            $"Country: {reader.GetValue(3)}\n" +
                                            $"Postal Code: {reader.GetValue(4)}\n" +
                                            $"Phone No.: {reader.GetValue(5)}\n" +
                                            $"Email: {reader.GetValue(6)}\n");
                                    }
                                }
                                catch
                                {
                                    Console.WriteLine($"Customer with name {name} does not exist.");
                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that adds a customer to the database.
        /// </summary>
        /// <param name="customer">A Customer object containing all data of the new customer.</param>
        public void AddCustomer(Customer customer)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {

                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");


                    string writeSQL = $"INSERT INTO Customer ({GetCustomerDataQueryString()}) VALUES (@FirstName, @LastName, @Country, @PostalCode, @PhoneNumber, @Email)";

                    try
                    {
                        using (SqlCommand command = new SqlCommand(writeSQL, connection))
                        {
                            command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                            command.Parameters.AddWithValue("@LastName", customer.LastName);
                            command.Parameters.AddWithValue("@Country", customer.Country);
                            command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                            command.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                            command.Parameters.AddWithValue("@Email", customer.Email);

                            command.ExecuteNonQuery();
                        }
                        UpdateDBCount();
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that retrieves the most popular genres of a customer in descending order.
        /// </summary>
        public void CustomerMostPopularGenre()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");

                    Console.WriteLine("Enter Id:");
                    string input = Console.ReadLine();
                    Int32.TryParse(input, out int id);

                    try
                    {
                        string readSQL =
                            $"SELECT TOP 2 Genre.Name, COUNT(Genre.Name) FROM Customer\n" +
                            $"INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId\n" +
                            $"INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId\n" +
                            $"INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId\n" +
                            $"INNER JOIN Genre ON Genre.GenreId = Track.GenreId\n" +
                            $"WHERE Customer.CustomerId = {id}\n" +
                            $"GROUP BY Customer.CustomerId, Genre.Name\n" +
                            $"ORDER BY COUNT(Genre.Name) DESC";

                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Console.WriteLine($"Genre {reader.GetValue(0)}: {reader.GetValue(1)}");
                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that retrieves the customer distribution per country, ordering the countries by amount of customers in that country.
        /// </summary>
        public void CustomerCountryDistribution()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");
                    try
                    {
                        string readSQL =
                            "SELECT COUNT(CustomerID), Country\n" +
                            "FROM Customer\n" +
                            "WHERE Country IS NOT NULL\n" +
                            "GROUP BY Country\n" +
                            "ORDER BY COUNT(CustomerID) DESC";

                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Console.WriteLine($"Country {reader.GetValue(1)}: {reader.GetValue(0)}");

                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that retrieves the expenditure of each customer ranking from highest to lowest.
        /// </summary>
        public void CustomerHighestSpender()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");
                    try
                    {
                        string readSQL =
                            "SELECT Customer.CustomerId, FirstName, LastName, SUM(Total)\n" +
                            "FROM Invoice\n" +
                            "INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId\n" +
                            "GROUP BY Customer.CustomerId, FirstName, LastName\n" +
                            "ORDER BY SUM(Total) DESC";

                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Console.WriteLine($"{reader.GetValue(1)} {reader.GetValue(2)} has spent {reader.GetValue(3)}");

                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong connecting to the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that retrieves all customers registered in the database or a range thereof.
        /// </summary>
        /// <param name="start">The ID of the customer to start retrieving data from.</param>
        /// <param name="total">Total amount of customers to view</param>
        public void GetAllCustomers(int start, int total = 0)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.connectionString))
                {
                    //Console.WriteLine("Attempting to connect to database.....);
                    connection.Open();
                    //Console.WriteLine("Connected");

                    try
                    {
                        string readSQL = $"SELECT CustomerId, {GetCustomerDataQueryString()} FROM Customer";
                        if (start != 0)
                            readSQL =
                                 $"SELECT * FROM Customer\n" +
                                 $"ORDER BY CustomerId\n" +
                                 $"OFFSET {start - 1} ROWS\n" +
                                 $"FETCH NEXT {total} ROWS ONLY";

                        using (SqlCommand command = new SqlCommand(readSQL, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                        $"Country: {reader.GetValue(3)}\n" +
                                        $"Postal Code: {reader.GetValue(4)}\n" +
                                        $"Phone No.: {reader.GetValue(5)}\n" +
                                        $"Email: {reader.GetValue(6)}\n");
                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine($"Something went wrong with the database: {e.Message}");
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Something went wrong with the database: {e.Message}");
            }
        }
        /// <summary>
        /// Helper function that retrieves a user from the database by ID.
        /// </summary>
        /// <param name="id">The desired ID of the customer.</param>
        public void GetCustomerById(int id)
        {
            if (id <= this.DbSize)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(this.connectionString))
                    {
                        //Console.WriteLine("Attempting to connect to database.....);
                        connection.Open();
                        //Console.WriteLine("Connected");

                        try
                        {
                            string readSQL = $"SELECT CustomerId, {GetCustomerDataQueryString()} FROM Customer WHERE CustomerID={id}";

                            using (SqlCommand command = new SqlCommand(readSQL, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                            $"Country: {reader.GetValue(3)}\n" +
                                            $"Postal Code: {reader.GetValue(4)}\n" +
                                            $"Phone No.: {reader.GetValue(5)}\n" +
                                            $"Email: {reader.GetValue(6)}\n");
                                    }
                                }
                            }
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine($"Something went wrong with the database: {e.Message}");
                        }
                        connection.Close();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
            }
            else
                Console.WriteLine($"That Id does not exist.");
        }
        /// <summary>
        /// Helper function that instantiates the desired columns to retrieve from the Database.
        /// </summary>
        /// <returns>A string containing all values of the desired columns to retrieve.</returns>
        public string GetCustomerDataQueryString()
        {
            return "FirstName, LastName, Country, PostalCode, Phone, Email";
        }
    }
}
