﻿namespace Assignment05.Models
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public Customer(string firstName, string lastName, string country, string postalCode, string phoneNumber, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Country = country;
            PostalCode = postalCode;
            PhoneNumber = phoneNumber;
            Email = email;
        }
    }
}
