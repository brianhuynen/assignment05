﻿using Microsoft.Data.SqlClient;
using Assignment05.Models;

namespace Assignment05
{ 
    public interface ICustomerRepository
    {
        public int GetDBSize();
        public void UpdateDBCount();
        public void HandleAddCustomer();
        public void HandleUpdateCustomer();
        public void HandleGetCustomer(string action);
        public void GetCustomerByName(string name);
        public void AddCustomer(Customer customer);
        public void CustomerMostPopularGenre();
        public void CustomerCountryDistribution();
        public void CustomerHighestSpender();
        public void GetAllCustomers(int start, int total);
        public void GetCustomerById(int id);
        public string GetCustomerDataQueryString();
    }
}
