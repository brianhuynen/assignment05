USE SuperheroDB

INSERT INTO Superpower(PowerName, PowerDescription)
VALUES('Shooting', 'Shooting firepower, grenade or rocket to enemy')

INSERT INTO Superpower(PowerName, PowerDescription)
VALUES('Turn Green', 'Turn green when getting angry and smash enemy')

INSERT INTO Superpower(PowerName, PowerDescription)
VALUES('Swing', 'Swing fast and from high place')

INSERT INTO Superpower(PowerName, PowerDescription)
VALUES('Shooting Silk', 'Shooting spider silk to hold and tie up enemy')

INSERT INTO Superpower(PowerName, PowerDescription)
VALUES('Fly', 'Can fly to space')



Declare @HeroName nvarchar(200) = 'Sam'
Declare @HeroAlias nvarchar(200) = 'Tester'
Declare @HeroOrigin nvarchar(200) = 'Indo'
Declare @PowerName nvarchar(200) = 'Invisibility'
Declare @PowerDesc nvarchar(200) = 'Can turn invisible'

Declare @PowerId int
Declare @HeroId int

-- If the hero already exists, use the existing hero ID

Select @HeroId = ID from [Superhero] where HeroName = @HeroName
Select @PowerId = PowerId from [Superpower] where PowerName = @PowerName

-- If the hero does not exist in the Heros table
If (@HeroId is null)
Begin
 -- Insert the hero
 Insert into Superhero values(@HeroName, @HeroAlias, @HeroOrigin)
 -- Get the Id of the student
 Select @HeroId = SCOPE_IDENTITY()
End

-- If the power does not exist in the Power table
If (@PowerId is null)
Begin
 -- Insert the power
 Insert into Superpower values(@PowerName, @PowerDesc)
 -- Get the Id of the power
 Select @PowerId = SCOPE_IDENTITY()
End

-- Insert HeroId & powerId in HeropowerRelationship table
Insert into HeroPowerRelationship values(@HeroId, @PowerId)

Insert into HeroPowerRelationship values(1, 1)

SELECT * FROM Superpower
SELECT * FROM HeroPowerRelationship


