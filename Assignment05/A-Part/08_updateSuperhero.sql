
USE SuperheroDB

UPDATE Superhero
SET HeroName='Jonny Stark'
WHERE ID = 1

UPDATE Superhero
SET Origin='Pluto', HeroName = 'Shazam'
WHERE ID = 4

SELECT * FROM Superhero