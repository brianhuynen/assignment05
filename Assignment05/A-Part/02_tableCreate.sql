
USE SuperheroDB

CREATE TABLE Superhero (
    ID int IDENTITY(1,1) PRIMARY KEY,
    HeroName NVARCHAR (200),
    Alias NVARCHAR (200),
    Origin NVARCHAR (200)
);

CREATE TABLE Assistant (
    AssistantID int IDENTITY(1,1) PRIMARY KEY,
    AssistantName NVARCHAR(200) NOT NULL UNIQUE
);

CREATE TABLE Superpower (
    PowerID int IDENTITY(1,1) PRIMARY KEY,
    PowerName NVARCHAR (200) NOT NULL,
    PowerDescription NVARCHAR (1000)
);

SELECT * FROM Superhero
SELECT * FROM Assistant
SELECT * FROM Superpower