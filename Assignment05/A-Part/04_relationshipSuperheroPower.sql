USE SuperheroDB

CREATE TABLE HeroPowerRelationship(
    HeroPowerId int IDENTITY(1,1) PRIMARY KEY,
    HeroId int,
    PowerId int,
    FOREIGN KEY (HeroId) REFERENCES Superhero(ID),
    FOREIGN KEY (PowerId) REFERENCES Superpower(PowerID)
);

SELECT * FROM HeroPowerRelationship
